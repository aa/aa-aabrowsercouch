/*
 * Copyright 2006-2011 the Active Archives contributors (see AUTHORS).
 * 
 * This file is part of Active Archives.
 * 
 * Closer Commenting is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * Closer Commenting is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * Closer Commenting.  If not, see <http://www.gnu.org/licenses/>.
 */


$(document).ready(function() {
    var db = $.couch.db("aabrowser");

    // binds form submit
    $('form#add_resource input[type="submit"]').click(function(e) {
        e.preventDefault();
        console.log("clicked");

        data = {
            'uri': $('form#add_resource input[type="text"]').val(),
        }

        $.ajax({
              url: '/_proxy/request_head',
              dataType: 'json',
              data: data, 
              success: function(data) {
                  db.saveDoc(
                      data,
                      {
                      success: function() { 
                          console.log("success");
                      },
                  });
              }
        });
    });


    // Retrieves the resources
    db.view('aabrowser/list_all', {  
        success: function(data) {  
            var elt = $('div#resource_list');
            for (i in data.rows) {
                var key = data.rows[i].key;
                var value = data.rows[i].value;
                var item = $('<div class="resource"></div>');
                item.append('<img src="img/snake.png" />');
                var detail = $('<dl></dl>');
                detail.append('<dt>file name</dt><dd>' + unescape(value['filename']) + '<dd>');
                detail.append('<dt>from</dt><dd>' + value['netloc'] + '<dd>');
                detail.append('<dt>last modified</dt><dd>' + value['header']['last-modified'] + '<dd>');
                item.append(detail);
                elt.append(item);
            }
        }
    });  
});

// vim: set foldmethod=indent :
