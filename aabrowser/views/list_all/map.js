/*
 * Retrieves all the resources
 */


function(doc) {
    if (doc.type === 'resource' && doc.uri) {
        emit(doc.uri, doc);
    };
}
