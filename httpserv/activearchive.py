import os.path
import httplib
try:
    import json
except:
    import simplejson as json
from bottle import (run, get, request, response)


@get('/request_head')
def request_head():
    mydict = {} 
    mydict['type'] = 'resource'
    uri = request.GET.get('uri')
    mydict['uri'] = uri
    url = httplib.urlsplit(uri) 
    mydict['netloc'] = url.netloc
    fn = os.path.split(url.path)[-1]
    mydict['filename'] = fn
    ext = os.path.splitext(fn)[-1]
    mydict['extension'] = ext
    conn = httplib.HTTPConnection(url.netloc)
    conn.request("HEAD", url.path)
    res = conn.getresponse()
    header = {}
    for t in res.getheaders():
        header[t[0]] = t[1]
    mydict['header'] = header
    response.content_type = 'application/json; charset=utf-8'
    return json.dumps(mydict, sort_keys=True, indent=4)


run(host='localhost', port=8080)
