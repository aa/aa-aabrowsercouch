import sys

try:
    # Python 2.6
    import json
except:
    # Prior to 2.6 reQUIRES SImplejson
    import simplejson as json

def requests():
    # 'for line in sys.stdin' won't work here
    line= sys.stdin.readline()
    while line:
        yield json.loads(line)
        line = sys.stdin.readline()

def respond(code=200, data={}, headers={}):
    sys.stdout.write("%s\n" % json.dumps({"code": code, "json": data, "headers": headers}))
    sys.stdout.flush()

def main():
    for req in requests():
        import httplib
        url = httplib.urlsplit(req["query"]["uri"]) 
        conn = httplib.HTTPConnection(url.netloc)
        conn.request("HEAD", url.path)
        res = conn.getresponse()
        mydict = {} 
        for t in res.getheaders():
            mydict[t[0]] = t[1]
        ##print res.status, res.reason
        ##print res.getheaders()
        #respond(data={"qs": req["query"]})
        respond(data=mydict)

if __name__ == "__main__":
    main()
